var React = require('react');

module.exports = React.createClass({
    handleReturnKey: function (e) {
        if (e.which === 13) {
            this.searchStr(e.target.value);
        }
    },
    handleSearchBtn: function (e) {
        var str = _.trim(e.target.value);
        if (!_.isEmpty(str)) {
            this.searchStr(str);
        }
    },
    searchStr: function (str) {
        //查找电影，TODO
    },
    render: function () {
        return (
            <div className="row search-box-container">
                <div className="col-sm-4 col-sm-offset-4">
                    <div className="search-box">
                        <form className="search-form">
                            <input className="form-control" onKeyDown = {this.handleReturnKey} placeholder="Search..." type="text"></input>
                            <button className="btn btn-link search-btn" onClick = {this.handleSearchBtn}>
                                <span className="glyphicon glyphicon-search"></span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
});