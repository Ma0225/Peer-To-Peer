var React = require('react');

var ModuleStore = require('../../../stores/ModuleStore');
var ModuleActions = require('../../../actions/ModuleActions');
var SandBox = require('../common/SandBoxModule.react');
var UserInfo = require('./UserInfoModule.react');

module.exports = React.createClass({
    getInitialState: function () {
        return {
            userInfo: {
                name: 'MockingBird',
                desc: '大家好， 我是balabala小魔仙，我的土逼名字叫二狗子...',
                countTwtter: 5,
                countLike: 27
            }
        };
    },
    render: function(){
        return (
            <SandBox>
                <UserInfo userInfo = {this.state.userInfo}/>
            </SandBox>
        );
    }
});