require("bootstrap/dist/css/bootstrap.min.css");

require("../../../less/Main.less");

var React = require('react');
var ReactRouter = require('react-router');
var classnames = require('classnames');
var tweenState = require('react-tween-state');

var Route = ReactRouter.Route;
var DefaultRoute = ReactRouter.DefaultRoute;
var RouteHandler = ReactRouter.RouteHandler;

var ModuleStore = require('../../stores/ModuleStore');
var RouterContainer = require('../../services/RouterContainer');
var ModuleConstant = require('../../constants/ModuleConstant');
var ModuleActions = require('../../actions/ModuleActions');
var env_prod = process.env.NODE_ENV === 'production';
var HomeModule, ProfileModule, MoviesModule, ChatModule, SettingsModule;
if (env_prod) {
    HomeModule = require('react-router-proxy?name=home!../modules/home/HomeModule.react');
    ProfileModule = require('react-router-proxy?name=profile!../modules/profile/ProfileModule.react');
    MoviesModule = require('react-router-proxy?name=movies!../modules/movies/MoviesModule.react');
    ChatModule = require('react-router-proxy?name=chat!../modules/chat/ChatModule.react');
    SettingsModule = require('react-router-proxy?name=settings!../modules/settings/SettingsModule.react');
} else {
    HomeModule = require('../modules/home/HomeModule.react');
    ProfileModule = require('../modules/profile/ProfileModule.react');
    MoviesModule = require('../modules/movies/MoviesModule.react');
    ChatModule = require('../modules/chat/ChatModule.react');
    SettingsModule = require('../modules/settings/SettingsModule.react');
}

var Header = require('../modules/common/Header.react');
var LeftNav = require('../modules/common/LeftNav.react');

var Main = React.createClass({
    _leftNavShowed: false,

    _hideleftNavTimeoutId: -1,

    mixins: [tweenState.Mixin],

    getInitialState: function () {
        return {
            currentModule: ModuleStore.getCurrentModule(),
            leftNavPosLeft: -100
        };
    },
    _onModuleChange: function () {
        this.setState({
            currentModule: ModuleStore.getCurrentModule()
        });
    },
    componentDidMount: function () {
        ModuleStore.addChangeListener(this._onModuleChange);
    },
    componentWillUnmount: function () {
        ModuleStore.removeChangeListener(this._onModuleChange);
    },

    gotoModule: function (moduleId) {
        ModuleActions.gotoModule(moduleId);
    },

    showLeftNav: function () {
        var that = this;
        if (that._hideleftNavTimeoutId > -1) {
            clearTimeout(that._hideleftNavTimeoutId);
            that._hideleftNavTimeoutId = -1;
        }
        if (that._leftNavShowed) {
            return;
        }
        that._leftNavShowed = true;
        that.tweenState('leftNavPosLeft', {
            easing: tweenState.easingTypes.easeInOutQuad,
            delay: 200,
            duration: 500,
            endValue: 0
        });
    },

    hideLeftNav: function () {
        var that = this;
        if (!that._leftNavShowed) {
            return;
        }
        that._hideleftNavTimeoutId = setTimeout(function () {
            that.tweenState('leftNavPosLeft', {
                easing: tweenState.easingTypes.easeInOutQuad,
                delay: 200,
                duration: 500,
                endValue: -100,
                onEnd: function () {
                    that._hideleftNavTimeoutId = -1;
                    that._leftNavShowed = false;
                }
            });
        }, 500);
    },

    render: function () {
        var that = this;
        var modules = _.chain(ModuleConstant.modules).values().map(function (module) {
            var id = module.id;
            var classStr = classnames('module-icon', id);
            return (
                <div key={id} className={classStr} onClick={that.gotoModule.bind(that, id)}><div className='icon'></div><span
                    className="sr-only">{module.label}</span></div>
            );
        }).value();
        return (
            <div className="container-fluid nopadding fullheight pos-relative">
                <LeftNav modules = {modules} leftNavPosLeft = {this.getTweeningValue('leftNavPosLeft')} showLeftNav = {this.showLeftNav} hideLeftNav = {this.hideLeftNav} />
                <Header currentModule = {this.state.currentModule} showLeftNav = {this.showLeftNav} hideLeftNav = {this.hideLeftNav} />
                <RouteHandler/>
            </div>
        );
    }
});

// declare our routes and their hierarchy
var routes = (
    <Route handler={Main}>
        <Route name="home" path="/home" handler={HomeModule}/>
        <Route name="device" path="/profile" handler={ProfileModule}/>
        <Route name="movies" path="/movies" handler={MoviesModule}/>
        <Route name="logs" path="/chat" handler={ChatModule}/>
        <Route name="settings" path="/settings" handler={SettingsModule}/>
        <DefaultRoute handler={HomeModule}/>
    </Route>
);

var router = ReactRouter.create({routes: routes, location: ReactRouter.HashLocation});
RouterContainer.setRouter(router);

router.run(function (Handler) {
    React.render(<Handler/>, document.body);
});
//React.render(<div>HelloWorld!</div>, document.body);